package com.andr3.tasks.service.listener

import com.andr3.tasks.service.model.HeaderModel

interface APIListener<T> {
    fun onSuccess(result: T, statusCode: Int)
    fun onFailure(message: String)
}