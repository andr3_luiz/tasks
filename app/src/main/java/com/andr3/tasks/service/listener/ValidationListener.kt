package com.andr3.tasks.service.listener

class ValidationListener(errorMessage: String = "") {

    private var mStatus: Boolean = true
    private var mMessage: String = ""
    
    init {
        if(errorMessage != ""){
            mStatus = false
            mMessage = errorMessage
        }
    }

    fun success() = mStatus
    fun failure() = mMessage
}