package com.andr3.tasks.service.respository

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.andr3.tasks.service.constants.TaskConstants
import com.google.gson.Gson

open class BaseRepository(context: Context) {

    val mContext: Context = context

    fun fail(code: Int) = code != TaskConstants.HTTP.SUCCESS

    fun failResponse(response: String): String{
        return Gson().fromJson(response, String::class.java)
    }

    /**
     * Verifica se existe conexão com a internet
     */

    fun isNetworkAvailable (context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            val nw = cm.activeNetwork ?: return false
            val actNw = cm.getNetworkCapabilities(nw) ?: return false
            return when{
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                //for other device how are able to connect with Ethernet
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                //for check internet over Bluetooth
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
        } else {
            return cm.activeNetworkInfo?.isConnected ?: false
        }
    }
}