package com.andr3.tasks.service.respository

import android.content.Context
import com.andr3.tasks.service.constants.TaskConstants
import com.andr3.tasks.service.model.PriorityModel
import com.andr3.tasks.service.respository.local.TaskDataBase
import com.andr3.tasks.service.respository.remote.PriorityService
import com.andr3.tasks.service.respository.remote.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PriorityRepository(context: Context): BaseRepository(context) {

    private val mRemote = RetrofitClient.createService(PriorityService::class.java)
    private val mDatabase = TaskDataBase.getDatabase(context).priorityDAO()


    fun all(){
        val call: Call<List<PriorityModel>> = mRemote.list()
        call.enqueue(object : Callback<List<PriorityModel>>{
            override fun onResponse(call: Call<List<PriorityModel>>, response: Response<List<PriorityModel>>) {
                if(response.code() == TaskConstants.HTTP.SUCCESS){
                    mDatabase.clear()
                    response.body()?.let { mDatabase.save(it) }
                }

            }

            override fun onFailure(p0: Call<List<PriorityModel>>, t: Throwable) {
            }

        })
    }

    fun list() = mDatabase.list()

    fun getDescription(id: Int) = mDatabase.getDescription(id)
}