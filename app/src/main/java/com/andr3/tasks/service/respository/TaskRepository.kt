package com.andr3.tasks.service.respository

import android.content.Context
import com.andr3.tasks.R
import com.andr3.tasks.service.listener.APIListener
import com.andr3.tasks.service.model.TaskModel
import com.andr3.tasks.service.respository.remote.RetrofitClient
import com.andr3.tasks.service.respository.remote.TaskService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TaskRepository(context: Context) : BaseRepository(context) {

        private val mRemote = RetrofitClient.createService(TaskService::class.java)

    fun all(listener: APIListener<List<TaskModel>>) {
        val call: Call<List<TaskModel>> = mRemote.all()
        listTasks(call, listener)
    }

        fun nextWeek(listener: APIListener<List<TaskModel>>) {
            val call: Call<List<TaskModel>> = mRemote.nextWeek()
            listTasks(call, listener)
        }

        fun expired(listener: APIListener<List<TaskModel>>) {
            val call: Call<List<TaskModel>> = mRemote.expired()
            listTasks(call, listener)
        }


    private fun listTasks(call: Call<List<TaskModel>>, listener: APIListener<List<TaskModel>>) {

        // Verificação de internet
        if (!isNetworkAvailable(mContext)) {
            listener.onFailure(mContext.getString(R.string.ERROR_INTERNET_CONNECTION))
            return
        }

        call.enqueue(object : Callback<List<TaskModel>> {
            override fun onFailure(call: Call<List<TaskModel>>, t: Throwable) {
                listener.onFailure(mContext.getString(R.string.ERROR_UNEXPECTED))
            }

            override fun onResponse(
                call: Call<List<TaskModel>>,
                response: Response<List<TaskModel>>
            ) {
                val code = response.code()
                if (fail(code)) {
                    listener.onFailure(failResponse(response.errorBody()!!.string()))
                } else {
                    response.body()?.let { listener.onSuccess(it, code) }
                }
            }
        })
    }

    fun updateStatus(id: Int, complete: Boolean, listener: APIListener<Boolean>) {

        // Verificação de internet
        if (!isNetworkAvailable(mContext)) {
            listener.onFailure(mContext.getString(R.string.ERROR_INTERNET_CONNECTION))
            return
        }

        // Decide entre atualização e remoção
        val call: Call<Boolean> = if (complete) {
            mRemote.complete(id)
        } else {
            mRemote.undo(id)
        }

        call.enqueue(object : Callback<Boolean> {
            override fun onFailure(call: Call<Boolean>, t: Throwable) {
                listener.onFailure(mContext.getString(R.string.ERROR_UNEXPECTED))
            }

            override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
                val code = response.code()
                if (fail(code)) {
                    listener.onFailure(failResponse(response.errorBody()!!.string()))
                } else {
                    response.body()?.let { listener.onSuccess(it, code) }
                }
            }

        })
    }

    fun delete(id: Int, listener: APIListener<Boolean>) {

        // Verificação de internet
        if (!isNetworkAvailable(mContext)) {
            listener.onFailure(mContext.getString(R.string.ERROR_INTERNET_CONNECTION))
            return
        }

        val call: Call<Boolean> = mRemote.delete(id)
        call.enqueue(object : Callback<Boolean> {
            override fun onFailure(call: Call<Boolean>, t: Throwable) {
                listener.onFailure(mContext.getString(R.string.ERROR_UNEXPECTED))
            }

            override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
                val code = response.code()
                if (fail(code)) {
                    listener.onFailure(failResponse(response.errorBody()!!.string()))
                } else {
                    response.body()?.let { listener.onSuccess(it, code) }
                }
            }
        })
    }

    fun load(id: Int, listener: APIListener<TaskModel>) {

        // Verificação de internet
        if (!isNetworkAvailable(mContext)) {
            listener.onFailure(mContext.getString(R.string.ERROR_INTERNET_CONNECTION))
            return
        }

        val call: Call<TaskModel> = mRemote.load(id)
        call.enqueue(object : Callback<TaskModel> {
            override fun onFailure(call: Call<TaskModel>, t: Throwable) {
                listener.onFailure(mContext.getString(R.string.ERROR_UNEXPECTED))
            }

            override fun onResponse(call: Call<TaskModel>, response: Response<TaskModel>) {
                val code = response.code()
                if (fail(code)) {
                    listener.onFailure(failResponse(response.errorBody()!!.string()))
                } else {
                    response.body()?.let { listener.onSuccess(it, code) }
                }
            }
        })
    }

    fun create(task: TaskModel, listener: APIListener<Boolean>) {

        // Verificação de internet
        if (!isNetworkAvailable(mContext)) {
            listener.onFailure(mContext.getString(R.string.ERROR_INTERNET_CONNECTION))
            return
        }

        val call: Call<Boolean> = mRemote.create(
            task.priorityId,
            task.description,
            task.dueDate,
            task.complete
        )
        call.enqueue(object : Callback<Boolean> {
            override fun onFailure(call: Call<Boolean>, t: Throwable) {
                listener.onFailure(mContext.getString(R.string.ERROR_UNEXPECTED))
            }

            override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
                val code = response.code()
                if (fail(code)) {
                    listener.onFailure(failResponse(response.errorBody()!!.string()))
                } else {
                    response.body()?.let { listener.onSuccess(it, code) }
                }
            }
        })
    }

    fun update(task: TaskModel, listener: APIListener<Boolean>) {

        // Verificação de internet
        if (!isNetworkAvailable(mContext)) {
            listener.onFailure(mContext.getString(R.string.ERROR_INTERNET_CONNECTION))
            return
        }

        val call: Call<Boolean> = mRemote.update(
            task.id,
            task.priorityId,
            task.description,
            task.dueDate,
            task.complete
        )
        call.enqueue(object : Callback<Boolean> {
            override fun onFailure(call: Call<Boolean>, t: Throwable) {
                listener.onFailure(mContext.getString(R.string.ERROR_UNEXPECTED))
            }

            override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
                val code = response.code()
                if (fail(code)) {
                    listener.onFailure(failResponse(response.errorBody()!!.string()))
                } else {
                    response.body()?.let { listener.onSuccess(it, code) }
                }
            }
        })
    }
}