package com.andr3.tasks.service.respository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.andr3.tasks.service.model.PriorityModel

//DAO é quem faz acesso ao banco, usando as entidades
@Dao
interface PriorityDAO {

    @Insert
    fun save(list: List<PriorityModel>)

    @Query("SELECT * FROM Priority")
    fun list(): List<PriorityModel>

    @Query("DELETE FROM priority")
    fun clear()

    @Query("SELECT description FROM Priority WHERE id = :id")
    fun getDescription(id: Int): String
}