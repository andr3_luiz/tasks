package com.andr3.tasks.service.respository.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.andr3.tasks.service.model.PriorityModel

@Database(entities = [PriorityModel::class], version = 1)
abstract class TaskDataBase: RoomDatabase(){

    abstract fun priorityDAO(): PriorityDAO

    companion object{
        private lateinit var INSTANCE: TaskDataBase

        fun getDatabase(context: Context): TaskDataBase{
            if(!Companion::INSTANCE.isInitialized){
                synchronized(TaskDataBase::class){
                    INSTANCE = Room.databaseBuilder(context, TaskDataBase::class.java, "tasksDB")
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }
    }
}