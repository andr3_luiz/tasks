package com.andr3.tasks.service.respository.remote

import com.andr3.tasks.service.model.HeaderModel
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PersonService {
    @POST("Authentication/Login")
    @FormUrlEncoded
    fun login(@Field("e-mail")email: String,
              @Field("passsword") password: String
    ): Call<HeaderModel>

    @POST("Authentication/Create")
    @FormUrlEncoded
    fun create(@Field("name")name: String,
              @Field("email") email: String,
              @Field("password")password: String

    ): Call<HeaderModel>
}