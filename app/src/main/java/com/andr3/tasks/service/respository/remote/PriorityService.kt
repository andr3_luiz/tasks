package com.andr3.tasks.service.respository.remote

import com.andr3.tasks.service.model.PriorityModel
import retrofit2.Call
import retrofit2.http.GET

interface PriorityService {
    @GET("Priority")
    fun list(): Call<List<PriorityModel>>
}