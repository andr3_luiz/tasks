package com.andr3.tasks.service.respository.remote

import com.andr3.tasks.service.model.TaskModel
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.*
import java.util.*

interface TaskService {

    @GET("Task")
    fun all(): Call<List<TaskModel>>

    @GET("Task/Next7Days")
    fun nextWeek(): Call<List<TaskModel>>

    @GET("Task/Overdue")
    fun expired(): Call<List<TaskModel>>

    @GET("Task/{id}")
    fun load(@Path(value = "id", encoded = true)id: Int): Call<TaskModel>

    @POST("Task")
    @FormUrlEncoded
    fun create(
        @Field("PriorityId") priorityId: Int,
        @Field("Description") description: String,
        @Field("DueDate") dueDate: String,
        @Field("Complete") complete: Boolean
    ): Call<Boolean>

    @HTTP(method = "PUT", path = "Task", hasBody = true)
    @FormUrlEncoded
    fun update(
        @Field("Id") Id: Int,
        @Field("priorityId") priorityId: Int,
        @Field("Description") description: String,
        @Field("DueDate") dueDate: String,
        @Field("Complete") complete: Boolean
    ): Call<Boolean>

    @HTTP(method = "PUT", path = "Task/complete", hasBody = true)
    @FormUrlEncoded
    fun complete(@Field("Id") id: Int, ): Call<Boolean>

    @HTTP(method = "PUT", path = "Task/Undo", hasBody = true)
    @FormUrlEncoded
    fun undo(@Field("Id") id: Int, ): Call<Boolean>

    @HTTP(method = "DELETE", path = "Task/Undo", hasBody = true)
    @FormUrlEncoded
    fun delete(@Field("Id") id: Int, ): Call<Boolean>

}
