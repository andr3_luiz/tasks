package com.andr3.tasks.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.andr3.tasks.R
import com.andr3.tasks.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (supportActionBar != null) {
            supportActionBar!!.hide()
        }

        mViewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        //Incializa Eventos
        setListeners();
        observe()

        //Verifica se o usuário está logado
        verifyLoggerUser()
    }

    override fun onClick(v: View) {
        if(v.id == R.id.button_login){
            handleLogin()
        } else if(v.id == R.id.text_register){
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

    /**
     * Incializa os eventos de click
     */

    private fun setListeners(){
        button_login.setOnClickListener(this)
        text_register.setOnClickListener(this)
    }

    /**
     * Verifica se o usuário está logado
     */

    private fun verifyLoggerUser(){
        mViewModel.verifyLoggedUser()
    }

    /**
     * Observa ViewModel
     */

    private fun observe(){
        mViewModel.login.observe(this, Observer{
            if (it.success()){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                Toast.makeText(applicationContext, it.failure(), Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.loggedUser.observe(this, Observer {
            if (it){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })
    }

    /**
     * Autentica usuário
     */

    private fun handleLogin(){
        val email = edit_email.text.toString()
        val password = edit_password.text.toString()

        mViewModel.doLogin(email, password)
    }
}