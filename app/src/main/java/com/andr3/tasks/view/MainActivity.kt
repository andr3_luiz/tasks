package com.andr3.tasks.view

import android.content.Intent
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ui.*
import com.andr3.tasks.R
import com.andr3.tasks.viewmodel.MainViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    //    private lateinit var binding: ActivityMainBinding
    private lateinit var mViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (supportActionBar != null) {
            supportActionBar!!.hide()
        }

        //ViewModel
        mViewModel = ViewModelProvider(this)[MainViewModel::class.java]


        // Barra de nagegação
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // Botão para adição de tarefas
        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            startActivity(Intent(this, TaskFormActivity::class.java))
        }

        //Navegação
        setupNavigation()

        //Observadores
//        observe()

    }

//        override fun OnResume(){
//            mViewModel.loadUserName()
//            super.onResume()
//        }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun setupNavigation() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_all_tasks, R.id.nav_next_tasks, R.id.nav_expired, R.id.nav_logout
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        // Escuta ação de click para logout
//        navView.setNavigationItemSelectedListener {
//            if (it.itemId == androidx.navigation.ui.R.id.nav_logout) {
//                mViewModel.logout()
//            } else {
//                NavigationUI.onNavDestinationSelected(it, navController);
//                drawerLayout.closeDrawer(GravityCompat.START);
//            }
//            true
//        }
    }

//    private fun observe() {
//        mViewModel.logout.observe(this, Observer {
//            startActivity(Intent(this, LoginActivity::class.java))
//            finish()
//        })
//
//        mViewModel.userName.observe(this, Observer {
//            val navView = findViewById<NavigationView>(androidx.navigation.ui.R.id.nav_view)
//            val header = navView.getHeaderView(0)
//
//            header.findViewById<TextView>(androidx.navigation.ui.R.id.text_name).text = it
//        })
//    }


}