package com.andr3.tasks.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.andr3.tasks.R
import com.andr3.tasks.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mViewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        if (supportActionBar != null) {
            supportActionBar!!.hide()
        }

        mViewModel = ViewModelProvider(this)[RegisterViewModel::class.java]

        //Inicializa eventos
        listeners()
        observe()
    }

    override fun onClick(v: View) {
        val id = v.id
        if(id == R.id.button_register){

            val name = edit_name.text.toString()
            val email = edit_email.text.toString()
            val  password = edit_password.text.toString()

            mViewModel.create(name, email, password)
        }
    }

    private fun observe(){
            mViewModel.create.observe(this, Observer {
                if (it.success()) {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                } else {
                    Toast.makeText(applicationContext, it.failure(), Toast.LENGTH_SHORT).show()
                }
            })
        }


    private fun listeners(){
        button_register.setOnClickListener(this)
    }

}