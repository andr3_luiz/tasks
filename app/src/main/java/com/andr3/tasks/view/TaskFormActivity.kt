package com.andr3.tasks.view

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.andr3.tasks.R
import com.andr3.tasks.service.constants.TaskConstants
import com.andr3.tasks.service.model.TaskModel
import com.andr3.tasks.viewmodel.TaskFormViewModel
import kotlinx.android.synthetic.main.activity_task_form.*
import java.text.SimpleDateFormat
import java.util.*

class TaskFormActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener,
    View.OnClickListener{

    private lateinit var mViewModel: TaskFormViewModel
    private val mDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    private val mListPriority: MutableList<Int> = arrayListOf()
    private var mTaskId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_form)

        if (supportActionBar != null) {
            supportActionBar!!.hide()
        }

        mViewModel = ViewModelProvider(this)[TaskFormViewModel::class.java]

        listeners()
        observe()

        mViewModel.listPriorities()

        loadDataFormActivity()
    }

    private fun loadDataFormActivity(){
        val bundle = intent.extras
        if (bundle != null){
            mTaskId = bundle.getInt(TaskConstants.BUNDLE.TASKID)
            mViewModel.load(mTaskId)
        }
    }

    override fun onClick(v: View) {
        val id = v.id
        if (id == R.id.button_date) {
            showDatePicker()
        } else if (id == R.id.button_save_task) {
            handleSave()
        }
    }

    private fun handleSave(){
        val task = TaskModel().apply {
            this.id = mTaskId
            this.description = edit_description.text.toString()
            this.complete = check_complete.isChecked
            this.dueDate = button_date.text.toString()
            this.priorityId = mListPriority[spinner_priority.selectedItemPosition]
        }

        mViewModel.save(task)
    }

    private fun showDatePicker(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(this, this, year, month, day).show()
    }

    private fun observe(){
        mViewModel.priorityList.observe(this, {
            val list: MutableList<String> = arrayListOf()
            for(item in it){
                list.add(item.description)
                mListPriority.add(item.id)
            }

            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, list)
            spinner_priority.adapter = adapter
        })

        mViewModel.task.observe(this, androidx.lifecycle.Observer {
            edit_description.setText(it.description)
            check_complete.isChecked = it.complete
            spinner_priority.setSelection(getIndex(it.priorityId))
        })

        mViewModel.validation.observe(this, androidx.lifecycle.Observer{
            if(it.success()){
                Toast.makeText(this, "Sucesso!", Toast.LENGTH_SHORT).show()
            } else  {
                Toast.makeText(this, it.failure(), Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun getIndex(priorityId: Int): Int {
            var index = 0
            for(i in 0 until mListPriority.count())
                if(mListPriority[i] == priorityId){
                    index = i
                    break
                }
        return index
    }

    private fun listeners(){
        button_save_task.setOnClickListener(this)
        button_date.setOnClickListener(this)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)

        val str = mDateFormat.format(calendar.time)
        button_date.text = str
    }


}