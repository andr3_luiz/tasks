package com.andr3.tasks.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andr3.tasks.service.listener.APIListener
import com.andr3.tasks.service.listener.ValidationListener
import com.andr3.tasks.service.model.TaskModel
import com.andr3.tasks.service.respository.TaskRepository

class AllTasksViewModel(application: Application) : AndroidViewModel(application) {

    private val mValidation = MutableLiveData<ValidationListener>()
    val validation: LiveData<ValidationListener> = mValidation

    private val mTaskRepository = TaskRepository(application)
    private val mList = MutableLiveData<List<TaskModel>>()
    var taskList: LiveData<List<TaskModel>> = mList
    private var mTaskFilter = 0

    fun list(taskFilter: Int){
        mTaskFilter = taskFilter
        mTaskRepository.all(object : APIListener<List<TaskModel>>{
            override fun onSuccess(result: List<TaskModel>, statusCode: Int) {
                mList.value = result
            }

            override fun onFailure(message: String) {
                mList.value = arrayListOf()
            }

        })
    }
}