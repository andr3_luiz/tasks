package com.andr3.tasks.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andr3.tasks.service.model.HeaderModel
import com.andr3.tasks.service.constants.TaskConstants
import com.andr3.tasks.service.listener.APIListener
import com.andr3.tasks.service.listener.ValidationListener
import com.andr3.tasks.service.respository.PersonRepository
import com.andr3.tasks.service.respository.PriorityRepository
import com.andr3.tasks.service.respository.local.SecurityPreferences
import com.andr3.tasks.service.respository.remote.RetrofitClient

class LoginViewModel(application: Application): AndroidViewModel(application) {

    //Acesso a dados
    private val mSharedPreferences = SecurityPreferences(application)
    private val mPersonRepository = PersonRepository(application)
    private val mPriorityRepository = PriorityRepository(application)

    // Login usando API
    private val mLogin = MutableLiveData<ValidationListener>()
    var login: LiveData<ValidationListener> = mLogin

    //Login usando SharedPreferences
    private val mLoggedUser = MutableLiveData<Boolean>()
    val loggedUser: LiveData<Boolean> = mLoggedUser

    /**
     * Faz login usando API
     */
    fun doLogin(email: String, password: String){
        mPersonRepository.login(email, password, object : APIListener<HeaderModel>{
            override fun onSuccess(result: HeaderModel, statusCode: Int) {
                // Salvar dados do usuário no SharePreferences
                mSharedPreferences.store(TaskConstants.SHARED.PERSON_KEY, result.personKey)
                mSharedPreferences.store(TaskConstants.SHARED.TOKEN_KEY, result.token)
                mSharedPreferences.store(TaskConstants.SHARED.PERSON_NAME, result.name)

                RetrofitClient.addHeader(result.token, result.personKey)

                mLogin.value = ValidationListener()

            }

            override fun onFailure(message: String) {
                mLogin.value = ValidationListener(message)
            }
        }

        )}

    /**
     *Verifica se o usuário está logado
     */
    fun verifyLoggedUser(){
        val token = mSharedPreferences.get(TaskConstants.SHARED.TOKEN_KEY)
        val person = mSharedPreferences.get(TaskConstants.SHARED.PERSON_KEY)

        RetrofitClient.addHeader(token, person)

        val logged = (token != "" && person != "")

        if(!logged){
            mPriorityRepository.all()
        }
        mLoggedUser.value = logged
    }
}