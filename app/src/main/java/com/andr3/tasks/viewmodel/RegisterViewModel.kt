package com.andr3.tasks.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andr3.tasks.service.model.HeaderModel
import com.andr3.tasks.service.constants.TaskConstants
import com.andr3.tasks.service.listener.APIListener
import com.andr3.tasks.service.listener.ValidationListener
import com.andr3.tasks.service.respository.PersonRepository
import com.andr3.tasks.service.respository.local.SecurityPreferences

class RegisterViewModel(application: Application): AndroidViewModel(application) {

    //Criação usando API
    private val mCreate = MutableLiveData<ValidationListener>()
    var create: LiveData<ValidationListener> = mCreate

    //Acesso aos dados
    private val mSharedPreferences = SecurityPreferences(application)
    private val mPersonRepository = PersonRepository(application)

    fun create(name: String, email: String, password: String){
        mPersonRepository.create(name, email, password, object: APIListener<HeaderModel>{

            override fun onSuccess(result: HeaderModel, statusCode: Int)  {

                // Salvar dados do usuário no SharePreferences
                mSharedPreferences.store(TaskConstants.SHARED.PERSON_KEY, result.personKey)
                mSharedPreferences.store(TaskConstants.SHARED.TOKEN_KEY, result.token)
                mSharedPreferences.store(TaskConstants.SHARED.PERSON_NAME, result.name)

                mCreate.value = ValidationListener()
            }

            override fun onFailure(message: String) {
                mCreate.value = ValidationListener(message)
            }


        })
    }

}