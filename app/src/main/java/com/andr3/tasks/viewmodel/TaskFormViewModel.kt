package com.andr3.tasks.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andr3.tasks.service.listener.APIListener
import com.andr3.tasks.service.listener.ValidationListener
import com.andr3.tasks.service.model.PriorityModel
import com.andr3.tasks.service.model.TaskModel
import com.andr3.tasks.service.respository.PriorityRepository
import com.andr3.tasks.service.respository.TaskRepository

class TaskFormViewModel(application: Application): AndroidViewModel(application) {

    private val mTaskRepository = TaskRepository(application)
    private val mPriorityRepository = PriorityRepository(application)

    private val mPriorityList = MutableLiveData<List<PriorityModel>>()
    var priorityList: LiveData<List<PriorityModel>> = mPriorityList

    private val mValidation = MutableLiveData<ValidationListener>()
    val validation: LiveData<ValidationListener> = mValidation

    private val mTask = MutableLiveData<TaskModel>()
    var task: LiveData<TaskModel> = mTask



    fun listPriorities(){
        mPriorityList.value = mPriorityRepository.list()
    }

    fun save(task: TaskModel){
        mTaskRepository.create(task, object : APIListener<Boolean>{
            override fun onSuccess(result: Boolean, statusCode: Int) {
                mValidation.value = ValidationListener()
            }

            override fun onFailure(message: String) {
                mValidation.value = ValidationListener(message)
            }

        })

    }

    fun load(id: Int){
        mTaskRepository.load(id, object : APIListener<TaskModel>{
            override fun onSuccess(result: TaskModel, statusCode: Int) {
                    mTask.value = result
            }

            override fun onFailure(message: String) {
                mValidation.value = ValidationListener(message)
            }

        })
    }


}